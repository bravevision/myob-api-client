require 'myob/api/result_iterator'
require 'myob/api/models/inventory_item'
require 'myob/api/models/inventory_item_price_matrix'

module Myob
  module Api

    class Inventory

      class Item

        def initialize(json_data)
          @data = json_data
        end

      end

      def initialize(myob)
        @myob = myob
      end

      def items
        ResultIterator.new(@myob, Myob::Api::Models::InventoryItem, 'Inventory/Item/')
      end
      def find_item(uid)
        data = @myob.get_json('Inventory/Item/'<<uid)
        item = Myob::Api::Models::InventoryItem.new(@myob)
        item.from_json(data)
        item
      end
      def item_price_matrices
        ResultIterator.new(@myob, Myob::Api::Models::InventoryItemPriceMatrix, 'Inventory/ItemPriceMatrix/')
      end
      def find_item_price_matrix(uid)
        data = @myob.get_json('Inventory/ItemPriceMatrix/'<<uid)
        item = Myob::Api::Models::InventoryItemPriceMatrix.new(@myob)
        item.from_json(data)
        item
      end
    end

  end
end
