require "myob/api/client/version"

module Myob
  module Api
    class ResultIterator
      include Enumerable

      def initialize(myob, klass, path)
        @myob     = myob
        @path     = path
        @klass    = klass
        @at_once  = 200
        @limit    = Float::INFINITY
      end

      def filter(filters)
        @filters = filters
        self
      end

      def at_once(at_once)
        @at_once = at_once
        self
      end

      def limit(limit)
        @limit = limit
        self
      end

      def each

        skip = 0
        count = 1

        params = "$top=#{@at_once}"
        params << "&$filter=#{@filters}" if @filters

        while(skip < count && skip < @limit)
          results = @myob.get_json("#{@path}?#{params}&$skip=#{skip}")
          count = results[:Count].to_i
          results[:Items].each do |item|
            next unless skip < @limit
            row = @klass.new(@myob)
            row.from_json(item)
            yield row
            skip+=1
          end if results[:Items]
        end

      end
    end

  end
end
