require "myob/api/client/version"
require "myob/api/inventory"
require "myob/api/contact"
require "myob/api/order"

require 'myob/api/models/tax_code'

require "net/http"
require "json"

module Myob
  module Api
    class Client

      attr_reader :inventory, :contact, :order

      def tax_codes
        ResultIterator.new(self, Myob::Api::Models::TaxCode, 'TaxCode/')
      end

      def initialize(uri: 'http://localhost:8080/AccountRight/', password: nil, uuid: nil, auth_token: nil, api_key: nil)
        # raise 'base64 password is required' if password.nil?

        @uri = URI(uri)
        # @http = Net::HTTP.new(@uri.host, @uri.port)
        # @http.use_ssl = (@uri.scheme == "https")
        @headers = {
          'Accept' => 'application/json',
          'x-myobapi-version' => 'v2'
        }
        if password
          @headers['x-myobapi-cftoken'] = password
        end
        if auth_token
          @headers['x-myobapi-key'] = api_key
          @headers['Authorization'] = "Bearer #{auth_token}"
        end

        @uuid = uuid

        @inventory = Inventory.new(self)
        @contact   = Contact.new(self)
        @order     = Order.new(self)

      end



      def get_json(path)
        response = http.request_get("#{@uri.path}/#{@uuid}/#{path}",@headers)
        parse_response response
      end

      def post_json(path, data)
        response = http.request_post("#{@uri.path}/#{@uuid}/#{path}",JSON.dump(data),@headers)
        parse_response response
      end

      def put_json(path, data)
        # puts "#{@uri.path}#{@uuid}/#{path}", JSON.dump(data)
        response = http.request_put("#{@uri.path}#{@uuid}/#{path}",JSON.dump(data),@headers)
        parse_response response
      end

      def parse_response(response)
        ret = JSON.parse(response.body, symbolize_names: true)

        if response.code.to_i >= 300
          puts response.body
          errors = ret[:Information]
          ret[:Errors].each do |error|
            errors << "\n#{error[:ErrorCode]} #{error[:Name]} : #{error[:Message]} #{error[:AdditionalDetails]}"
          end if ret[:Errors]
          raise errors || response.body
        end

        ret
      end

      private

      def http
        Thread.current[:myob_http] ||= new_http
      end

      def new_http
        h = Net::HTTP.new(@uri.host, @uri.port)
        h.use_ssl = (@uri.scheme == "https")
        h
      end

    end
  end
end
