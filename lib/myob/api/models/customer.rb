require 'myob/api/result_iterator'

module Myob
  module Api
    module Models

      class Customer

        attr_accessor :uid, :company_name, :first_name, :last_name,
                      :is_individual, :is_active, :display_id, :addresses,
                      :notes, :current_balance, :selling_details, :payment_details,
                      :photo_uri, :uri, :row_version, :persisted, :tax_code_uid,
                      :freight_tax_code_uid

        def from_json(data)
          @data = data

          @uid                = data[:UID]
          @company_name       = data[:CompanyName]
          @last_name          = data[:LastName]
          @first_name         = data[:FirstName]
          @is_individual      = data[:IsIndividual]
          @display_id         = data[:DisplayID]
          @is_active          = data[:IsActive]
          @addresses          = []
          if data[:Addresses]
            data[:Addresses].each do |address|
              @addresses << {
                location: address[:Location],
                street: address[:Street],
                city: address[:City],
                state: address[:State],
                post_code: address[:PostCode],
                country: address[:Country],
                phone1: address[:Phone1],
                phone2: address[:Phone3],
                phone3: address[:Phone3],
                fax: address[:Fax],
                email: address[:Email],
                website: address[:Website],
                contact_name: address[:ContactName],
                salutation: address[:Salutation]
              }
            end
          end

          @notes              = data[:Notes]
          @current_balance    = data[:CurrentBalance]

          sd = data[:SellingDetails]
          if sd
            @selling_details = {
              sale_layout: sd[:SaleLayout],
              printed_form: sd[:PrintedForm],
              invoice_delivery: sd[:InvoiceDelivery],
              item_price_level: sd[:ItemPriceLevel],
              receipt_memo: sd[:ReceiptMemo],
              sale_comment: sd[:SaleComment],
              shipping_method: sd[:ShippingMethod],
              hourly_billing_rate: sd[:HourlyBillingRate],
              abn: sd[:ABN],
              abn_branch: sd[:ABNBranch],
              tax_code: {
                uid: sd[:TaxCode][:UID],
                code: sd[:TaxCode][:Code],
                uri: sd[:TaxCode][:URI]
              },
              freight_tax_code: {
                uid: sd[:FreightTaxCode][:UID],
                code: sd[:FreightTaxCode][:Code],
                uri: sd[:FreightTaxCode][:URI]
              },
              use_customer_tax_code: sd[:UseCustomerTaxCode],
              terms: {
                payment_is_due: sd[:Terms][:PaymentIsDue],
                discount_date: sd[:Terms][:DiscountDate],
                balance_due_date: sd[:Terms][:BalanceDueDate],
                discount_for_early_payment: sd[:Terms][:DiscountForEarlyPayment],
                monthly_charge_for_late_payment: sd[:Terms][:MonthlyChargeForLatePayment],
                volume_discount: sd[:Terms][:VolumeDiscount],
                tax_id_number: sd[:Terms][:TaxIDNumber],
                memo: sd[:Terms][:Memo]
              }
            }
          end

          if sd[:IncomeAccount]
            @selling_details[:income_account] = {
              uid: sd[:IncomeAccount][:UID],
              name: sd[:IncomeAccount][:Name],
              display_id: sd[:IncomeAccount][:DisplayID],
              uri: sd[:IncomeAccount][:URI]
            }
          end

          if sd[:SalesPerson]
            @selling_details[:sales_person] = {
              uid: sd[:SalesPerson][:UID],
              name: sd[:SalesPerson][:Name],
              display_id: sd[:SalesPerson][:DisplayID],
              uri: sd[:SalesPerson][:URI]
            }
          end

          if sd[:Credit]
            @selling_details[:credit] = {
              limit: sd[:Credit][:Limit],
              available: sd[:Credit][:Available],
              past_due: sd[:Credit][:PastDue],
              on_hold: sd[:Credit][:OnHold]
            }
          end

          if data[:PaymentDetails]
            @payment_details = {
              method: data[:PaymentDetails][:Method],
              card_number: data[:PaymentDetails][:CardNumber],
              name_on_card: data[:PaymentDetails][:NameOnCard],
              bsb_number: data[:PaymentDetails][:BSBNumber],
              bank_account_number: data[:PaymentDetails][:BankAccountNumber],
              bank_account_name: data[:PaymentDetails][:BankAccountName],
              notes: data[:PaymentDetails][:Notes]
            }
          end

          @last_modified      = data[:LastModified]
          @photo_uri          = data[:PhotoURI]
          @uri                = data[:URI]
          @row_version        = data[:RowVersion]
          @persisted          = true
        end

        def save
          data = {
            CompanyName:              @company_name,
            LastName:                 @last_name[0...30],
            FirstName:                @first_name[0...20],
            IsIndividual:             @is_individual,
            DisplayID:                @display_id,
            SellingDetails: {
              TaxCode: {
                UID: @tax_code_uid
              },
              FreightTaxCode: {
                UID: @freight_tax_code_uid
              }
            }
          }
          if @addresses && @addresses.size > 0
            data[:Addresses] = @addresses.map do |address|
              {
                Location: address[:location],
                Street: address[:street],
                City: address[:city],
                State: address[:state],
                PostCode: address[:post_code],
                Country: address[:country],
                Phone1: address[:phone1],
                Phone2: address[:phone2],
                Phone3: address[:phone3],
                Fax: address[:fax],
                Email: address[:email],
                Website: address[:website],
                ContactName: address[:contact_name],
                Salutation: address[:salutation]
              }
            end
          end
          if @persisted
            data[:RowVersion] =   @row_version
            data[:UID]        =   @uid
          end
          data = @myob.post_json('Contact/Customer?returnBody=true', data)
          from_json(data)
        end

        def initialize(myob)
          @persisted = false
          @myob = myob
        end

      end

    end

  end
end
