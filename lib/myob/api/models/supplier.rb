require 'myob/api/result_iterator'

module Myob
  module Api
    module Models
      class Supplier

        attr_accessor :uid, :company_name, :first_name, :last_name,
                      :is_individual, :is_active, :display_id, :addresses,
                      :notes, :current_balance, :buying_details, :payment_details,
                      :photo_uri, :uri, :row_version, :persisted, :tax_code_uid,
                      :freight_tax_code_uid

        def from_json(data)
          @data = data

          @uid                = data[:UID]
          @company_name       = data[:CompanyName]
          @last_name          = data[:LastName]
          @first_name         = data[:FirstName]
          @is_individual      = data[:IsIndividual]
          @display_id         = data[:DisplayID]
          @is_active          = data[:IsActive]
          @addresses          = []
          data[:Addresses].each do |address|
            @addresses << {
                location: address[:Location],
                street: address[:Street],
                city: address[:City],
                state: address[:State],
                post_code: address[:PostCode],
                courntry: address[:Country],
                phone1: address[:Phone1],
                phone2: address[:Phone3],
                phone3: address[:Phone3],
                fax: address[:Fax],
                email: address[:Email],
                website: address[:Website],
                contact_name: address[:ContactName],
                salutation: address[:Salutation]
            }
          end if data[:Addresses]
          @notes              = data[:Notes]
          @current_balance    = data[:CurrentBalance]

          bd = data[:BuyingDetails]
          @buying_details = {
              purchase_layout: bd[:PurchaseLayout],
              printed_form: bd[:PrintedForm],
              purchase_order_delivery: bd[:PurchaseOrderDelivery],

              # ExpenseAccount

              payment_memo: bd[:PaymentMemo],
              purchase_comment: bd[:PurchaseComment],
              supplier_billing_rate: bd[:SupplierBillingRate],
              shipping_method: bd[:ShippingMethod],
              is_reportable: bd[:IsReportable],
              cost_per_hour: bd[:CostPerHour],
              # Credit
              abn: bd[:ABN],
              abn_branch: bd[:ABNBranch],
              tax_id_number: bd[:TaxIdNumber],
              tax_code: {
                  uid: bd[:TaxCode][:UID],
                  code: bd[:TaxCode][:Code],
                  uri: bd[:TaxCode][:URI]
              },
              freight_tax_code: {
                  uid: bd[:FreightTaxCode][:UID],
                  code: bd[:FreightTaxCode][:Code],
                  uri: bd[:FreightTaxCode][:URI]
              },
              use_suppliers_tax_code: bd[:UseSuppliersTaxCode],
              terms: {
                  payment_is_due: bd[:Terms][:PaymentIsDue],
                  discount_date: bd[:Terms][:DiscountDate],
                  balance_due_date: bd[:Terms][:BalanceDueDate],
                  discount_for_early_payment: bd[:Terms][:DiscountForEarlyPayment],
                  volume_discount: bd[:Terms][:VolumeDiscount],
              }
          } if bd
          # @selling_details[:income_account] = {
          #     uid: bd[:IncomeAccount][:UID],
          #     name: bd[:IncomeAccount][:Name],
          #     display_id: bd[:IncomeAccount][:DisplayID],
          #     uri: bd[:IncomeAccount][:URI]
          # } if bd[:IncomeAccount]
          #
          # @selling_details[:sales_person] = {
          #     uid: bd[:SalesPerson][:UID],
          #     name: bd[:SalesPerson][:Name],
          #     display_id: bd[:SalesPerson][:DisplayID],
          #     uri: bd[:SalesPerson][:URI]
          # } if bd[:SalesPerson]
          #
          # @selling_details[:credit] = {
          #     limit: bd[:Credit][:Limit],
          #     available: bd[:Credit][:Available],
          #     past_due: bd[:Credit][:PastDue],
          #     on_hold: bd[:Credit][:OnHold],
          # } if bd[:Credit]


          # @payment_details    = {
          #     method: data[:PaymentDetails][:Method],
          #     card_number: data[:PaymentDetails][:CardNumber],
          #     name_on_card: data[:PaymentDetails][:NameOnCard],
          #     bsb_number: data[:PaymentDetails][:BSBNumber],
          #     bank_account_number: data[:PaymentDetails][:BankAccountNumber],
          #     bank_account_name: data[:PaymentDetails][:BankAccountName],
          #     notes: data[:PaymentDetails][:Notes]
          # } if data[:PaymentDetails]

          @last_modified      = data[:LastModified]
          @photo_uri          = data[:PhotoURI]
          @uri                = data[:URI]
          @row_version        = data[:RowVersion]
          @persisted          = true
        end

        def save
          data = {
              CompanyName:              @company_name,
              LastName:                 @last_name[0...30],
              FirstName:                @first_name[0...20],
              IsIndividual:             @is_individual,
              DisplayID:                @display_id,
              BuyingDetails: {
                  TaxCode: {
                      UID: @tax_code_uid
                  },
                  FreightTaxCode: {
                      UID: @freight_tax_code_uid
                  }
              }

          }
          if @persisted
            data[:RowVersion] =   @row_version
            data[:UID]        =   @uid
          end
          data = @myob.post_json('Contact/Supplier?returnBody=true', data)
          from_json(data)
        end

        def initialize(myob)
          @persisted = false
          @myob = myob
        end

      end

    end

  end
end
