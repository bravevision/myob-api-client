require 'myob/api/result_iterator'

module Myob
  module Api
    module Models

      class TaxCode

        attr_accessor :uid, :description, :type, :rate, :data, :row_version, :persisted

        def from_json(data)
          @data = data

          @description  = data[:Description]
          @type         = data[:Type]
          @rate         = data[:Rate]

          @uid                = data[:UID]
          @row_version        = data[:RowVersion]
          @persisted = true
        end

        def save
          raise 'Unable to save!'
        end

        def initialize(myob)
          @persisted = false
          @myob = myob
        end

      end

    end

  end
end
