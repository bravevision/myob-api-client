require 'myob/api/result_iterator'



module Myob
  module Api
    module Models

      class InventoryItem

        attr_reader :uid, :quantity_on_hand, :quantity_committed, :quantity_on_order, :quantity_available,
                    :base_selling_price, :current_value, :average_cost,
                    :selling_details, :buying_details,
                    :persisted

        attr_accessor :number, :name, :is_active, :is_bought, :is_sold, :is_inventoried, :use_description, :description,
                      :custom_list_1, :custom_list_2, :custom_list_3, :custom_field_1, :custom_field_2, :custom_field_3,
                      :tax_code_uid, :cost_of_sales_uid, :income_account_uid, :asset_account_uid

        def from_json(data)
          @data = data

          # puts data.inspect

          @uid                = data[:UID]
          @number             = data[:Number]
          @name               = data[:Name]
          @is_active          = data[:IsActive]
          @is_bought          = data[:IsBought]
          @is_sold            = data[:IsSold]
          @is_inventoried     = data[:IsInventoried]
          @use_description    = data[:UseDescription]
          @description        = data[:Description]
          @quantity_on_hand   = data[:QuantityOnHand]
          @quantity_committed = data[:QuantityCommitted]
          @quantity_on_order  = data[:QuantityOnOrder]
          @quantity_available = data[:QuantityAvailable]
          @average_cost       = data[:AverageCost]
          @current_value      = data[:CurrentValue]
          @base_selling_price = data[:BaseSellingPrice]

          @custom_list_1      = data[:CustomList1] && data[:CustomList1][:Value]
          @custom_list_2      = data[:CustomList2] && data[:CustomList2][:Value]
          @custom_list_3      = data[:CustomList3] && data[:CustomList3][:Value]
          @custom_field_1     = data[:CustomField1] && data[:CustomField1][:Value]
          @custom_field_2     = data[:CustomField2] && data[:CustomField2][:Value]
          @custom_field_3     = data[:CustomField3] && data[:CustomField3][:Value]

          @selling_details    = SellingDetails.new(data[:SellingDetails])
          @buying_details     = BuyingDetails.new(data[:BuyingDetails])

          @cost_of_sales_uid  = data[:CostOfSalesAccount][:UID] if data[:CostOfSalesAccount]
          @income_account_uid = data[:IncomeAccount][:UID] if data[:IncomeAccount]
          @asset_account_uid  = data[:AssetAccount][:UID] if data[:AssetAccount]

          @row_version        = data[:RowVersion]

          @persisted = true
          
        end

        def initialize(myob)
          @myob = myob
          @persisted = false
          @selling_details    = SellingDetails.new()
          @buying_details     = BuyingDetails.new()
        end

        def save
          data = {
            Number:               @number,
            Name:                 @name,
            IsActive:             @is_active || true,
            IsBought:             @is_bought || true,
            IsSold:               @is_sold || true,
            IsInventoried:        @is_inventoried || true,
            UseDescrption:        @use_description || true,
            Description:          @description
          }

          data[:CustomList1]  = {Value: @custom_list_1} unless @custom_list_1.nil?
          data[:CustomList2]  = {Value: @custom_list_2} unless @custom_list_2.nil?
          data[:CustomList3]  = {Value: @custom_list_3} unless @custom_list_3.nil?
          data[:CustomField1] = {Value: @custom_field_1} unless @custom_field_1.nil?
          data[:CustomField2] = {Value: @custom_field_2} unless @custom_field_2.nil?
          data[:CustomField3] = {Value: @custom_field_3} unless @custom_field_3.nil?

          data[:SellingDetails] = @selling_details.to_hash  if @selling_details && @selling_details.use_me?
          data[:BuyingDetails]  = @buying_details.to_hash   if @buying_details && @buying_details.use_me?

          data[:CostOfSalesAccount] = {UID: @cost_of_sales_uid} if @cost_of_sales_uid
          data[:IncomeAccount] = {UID: @income_account_uid} if @income_account_uid
          data[:AssetAccount] = {UID: @asset_account_uid} if @asset_account_uid

          if @persisted
            data[:UID]        = @uid
            data[:RowVersion] = @row_version
          end
          data = @myob.post_json('/Inventory/Item?returnBody=true', data)
          from_json(data)

        end

        class SellingDetails

          attr_reader :persisted

          attr_accessor :base_selling_price, :items_per_selling_unit, :selling_unit_of_measure, :tax_code_uid, :is_tax_inclusive, :calculate_sales_tax_on

          def initialize(data=nil)
            if data
              from_hash(data)
              persisted = true
            end
          end

          def from_hash(data)
            @base_selling_price       = data[:BaseSellingPrice]
            @items_per_selling_unit   = data[:ItemsPerSellingUnit]
            @selling_unit_of_measure  = data[:SellingUnitOfMeasure]
            @tax_code_uid             = data[:TaxCode][:UID] if data[:TaxCode]
            @is_tax_inclusive         = data[:IsTaxInclusive]
            @calculate_sales_tax_on   = data[:CalculateSalesTaxOn]
          end

          def to_hash
            ret = {}
            ret[:BaseSellingPrice] = @base_selling_price if @base_selling_price
            ret[:ItemsPerSellingUnit] = @items_per_selling_unit if @items_per_selling_unit
            ret[:SellingUnitOfMeasure] = @selling_unit_of_measure if @selling_unit_of_measure
            ret[:TaxCode] = {UID: @tax_code_uid} if @tax_code_uid
            ret[:IsTaxInclusive] = @is_tax_inclusive if @is_tax_inclusive
            ret[:CalculateSalesTaxOn] = @calculate_sales_tax_on if @calculate_sales_tax_on
            ret
          end

          def use_me?
            @base_selling_price || @items_per_selling_unit || @selling_unit_of_measure || @tax_code_uid || @is_tax_inclusive || @calculate_sales_tax_on
          end
        end

        class BuyingDetails

          attr_reader :persisted

          attr_accessor :last_purchase_price, :standard_cost, :items_per_buying_unit, :buying_unit_of_measure, :tax_code_uid, :restocking_information

          def initialize(data=nil)
            if data
              from_hash(data)
              persisted = true
            else
              @restocking_information = RestockingInformation.new
            end
          end

          def from_hash(data)
            @last_purchase_price    = data[:LastPurchasePrice]
            @standard_cost          = data[:StandardCost]
            @items_per_buying_unit  = data[:ItemsPerBuyingUnit]
            @buying_unit_of_measure = data[:BuyingUnitOfMeasure]
            @tax_code_uid           = data[:TaxCode][:UID] if data[:TaxCode]
            @restocking_information   = RestockingInformation.new(data[:RestockingInformation])
          end

          def to_hash
            ret = {}
            ret[:LastPurchasePrice] = @last_purchase_price if @last_purchase_price
            ret[:StandardCost] = @standard_cost if @standard_cost
            ret[:ItemsPerBuyingUnit] = @items_per_buying_unit if @items_per_buying_unit
            ret[:BuyingUnitOfMeasure] = @buying_unit_of_measure if @buying_unit_of_measure
            ret[:TaxCode] = {UID: @tax_code_uid} if @tax_code_uid
            ret[:RestockingInformation]  = @restocking_information.to_hash   if @restocking_information && @restocking_information.use_me?
            ret
          end

          def use_me?
            @last_purchase_price || @standard_cost || @items_per_buying_unit || @buying_unit_of_measure || @tax_code_uid || @restocking_information
          end

          class RestockingInformation

            # SupplierItemNumber DefaultOrderQuantity MinimumLevelForRestockingAlert Supplier
            attr_accessor :supplier_item_number, :default_order_quantity, :minimum_level_for_restocking_alert, :supplier

            attr_reader :persisted

            def initialize(data=nil)
              if data
                from_hash(data)
                persisted = true
              else
                @supplier = Supplier.new
              end
            end

            def from_hash(data)
              @supplier_item_number              = data[:SupplierItemNumber]
              @default_order_quantity            = data[:DefaultOrderQuantity]
              @minimum_level_for_restocking_alert = data[:MinimumLevelForRestockingAlert]
              @supplier                           = Supplier.new(data[:Supplier])
            end

            def to_hash
              ret = {}
              ret[:SupplierItemNumber] = @supplier_item_number if @supplier_item_number
              ret[:DefaultOrderQuantity] = @default_order_quantity if @default_order_quantity
              ret[:MinimumLevelForRestockingAlert] = @minimum_level_for_restocking_alert if @minimum_level_for_restocking_alert
              ret[:Supplier] = @supplier.to_hash if @supplier && @supplier.use_me?
              ret
            end

            def use_me?
              @supplier_item_number || @default_order_quantity || @minimum_level_for_restocking_alert || @supplier
            end

            class Supplier
              attr_accessor :uid, :name, :display_id, :uri
              attr_reader :persisted

              def initialize(data=nil)
                if data
                  from_hash(data)
                  persisted = true
                end
              end

              def from_hash(data)
                @uid = data[:UID]
                @name = data[:Name]
                @display_id = data[:DisplayID]
                @uri = data[:uri]
              end

              def to_hash
                ret = {}
                ret[:UID] = @uid if @uid
                ret[:DisplayID] = @display_id if @display_id
                ret
              end

              def use_me?
                @uid || @display_id
              end

            end

          end

        end

      end
    end
  end
end
