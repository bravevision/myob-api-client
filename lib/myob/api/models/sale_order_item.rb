require 'myob/api/result_iterator'

module Myob
  module Api
    module Models
      class SaleOrderItem

        attr_accessor :uid, :number, :date, :ship_to_address, :customer_purchase_order_number,
                      :customer, :terms, :is_tax_inclusive, :lines, :sales_person, :comment,
                      :shipping_method, :journal_memo, :promised_date, :delivery_status, :referral_source,
                      :applied_to_date, :balance_due_amount, :status, :last_payment_date, :uri, :subtotal, :row_version,
                      :data, :persisted, :freight, :freight_tax_code, :total_tax, :total_amount, :category


        def from_json(data)
          @data = data

          # puts data.inspect

          @uid                            = data[:UID]
          @number                         = data[:Number]
          @date                           = data[:Date]
          @ship_to_address                = data[:ShipToAddress]
          @customer_purchase_order_number = data[:CustomerOrderNumber]
          @customer                       = {
              uid:        data[:Customer][:UID],
              name:       data[:Customer][:Name],
              display_id: data[:Customer][:Display_ID],
              # uri:        data[:Customer][:URI]
          }
          @terms                          = {
              payment_is_due:                  data[:Terms][:PaymentIsDue],
              discount_date:                   data[:Terms][:DiscountDate],
              balance_due_date:                data[:Terms][:BalanceDueDate],
              discount_for_early_payment:      data[:Terms][:DiscountForEarlyPayment],
              monthly_charge_for_late_payment: data[:Terms][:MonthlyChargeForLatePayment],
              discount_expiry_date:            data[:Terms][:DiscountExpiryDate],
              discount:                        data[:Terms][:Discount],
              due_date:                        data[:Terms][:DueDate],
              finance_charge:                  data[:Terms][:FinanceChange]
          }
          @is_tax_inclusive               = data[:IsTaxInclusive]
          @lines                          = []
          data[:Lines].each do |line|
            @lines << {
                row_id:              line[:RowID],
                type:                line[:Type],
                description:         line[:Description],
                ship_quantity:       line[:ShipQuantity],
                unit_price:          line[:UnitPrice],
                discount_percent:    line[:DiscountPercent],
                total:               line[:total],

                item:                line[:Item].nil?() ? nil : {
                    uid:      line[:Item][:UID],
                    number:   line[:Item][:Number],
                    name:     line[:Item][:Name],
                },

                # job:                 {
                #     uid:      line[:Job][:UID],
                #     number:   line[:Job][:Number],
                #     name:     line[:Job][:Name]
                #
                # },

                tax_code:            line[:TaxCode].nil?() ? nil :{
                    uid:      line[:TaxCode][:UID],
                    code:     line[:TaxCode][:Code]
                },
                row_version:          line[:RowVersion]
            }

          end
          @subtotal                     = data[:Subtotal]
          @freight                      = data[:Freight]
          @freight_tax_code             = data[:FreightTaxCode]
          @total_tax                    = data[:TotalTax]
          @total_amount                 = data[:TotalAmount]
          @category                     = {
              uid:        data[:Category][:UID],
              name:       data[:Category][:Name],
              display_id: data[:Category][:DisplayID]
          } if data[:Category]

          @sales_person                  = {
              uid:        data[:Salesperson][:UID],
              name:       data[:Salesperson][:Name],
              display_id: data[:Salesperson][:DisplayID]
          } if data[:Salesperson]
          @comment                       = data[:Comment]
          @shipping_method               = data[:ShippingMethod]
          @journal_memo                  = data[:JournalMemo]
          @promised_date                 = data[:PromisedDate]
          @delivery_status               = data[:DeliveryStatus]
          @referral_source               = data[:ReferralSource]
          @applied_to_date               = data[:AppliedToDate]
          @balance_due_amount            = data[:BalanceDueAmount]
          @row_version                   = data[:RowVersion]
          @persisted                     = true
        end

        def save
          data = {
            Number:                      @number,
            Date:                        @date,
            ShipToAddress:               @ship_to_address,
            CustomerPurchaseOrderNumber: @customer_purchase_order_number,
            Customer:                    {
                UID: @customer[:uid],
                # DisplayID: @customer[:display_id]
            },
            Terms:                       @terms,
            IsTaxInclusive:              @is_tax_inclusive,
            Lines:                       @lines,
            # Subtotal:                    @subtotal,
            Freight:                     @freight,
            FreightTaxCode:              @freight_tax_code,
            # TotalTax:                    @total_tax,
            # TotalAmount:                 @total_amount,
            Category:                    @category,
            SalesPerson:                 @sales_person,
            Comment:                     @comment,
            ShippingMethod:              @shipping_method,
            JournalMemo:                 @journal_memo,
            PromisedDate:                @promised_date,
            DeliveryStatus:              @delivery_status,
            ReferralSource:              @referral_source,
            # AppliedToDate:               @applied_to_date,
            # BalanceDueAmount:            @balance_due_amount,


          }
          if @persisted
            data[:RowVersion] =   @row_version
            data[:UID]        =   @uid
          end
          data = @myob.post_json('Sale/Order/Item?returnBody=true', data)
          from_json(data)
        end
        def initialize(myob)
          @persisted = false
          @myob = myob
        end
      end
    end
  end
end