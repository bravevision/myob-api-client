require 'myob/api/result_iterator'

module Myob
  module Api
    module Models

      class InventoryItemPriceMatrix

        attr_accessor :uid, :selling_prices, :data, :row_version, :persisted

        def from_json(data)
          @data = data
          @selling_prices     = data[:SellingPrices]

          @uid                = data[:UID]
          @row_version        = data[:RowVersion]
          @persisted = true
        end

        def save
          data = {
              SellingPrices: @selling_prices
          }

          if @persisted
            data[:UID]        = @uid
            data[:RowVersion] = @row_version
          end
          data = @myob.put_json("Inventory/ItemPriceMatrix/#{@uid}?returnBody=true", data)
          from_json(data)
        end

        def initialize(myob)
          @persisted = false
          @myob = myob
        end

      end

    end

  end
end
