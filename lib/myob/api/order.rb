require 'myob/api/result_iterator'
require 'myob/api/models/sale_order_item'



module Myob
  module Api
    class Order


      def initialize(myob)
        @myob = myob
      end

      def sale_order_item
        ResultIterator.new(@myob, Myob::Api::Models::SaleOrderItem, 'Sale/Order/Item/')
      end
    end

  end
end
