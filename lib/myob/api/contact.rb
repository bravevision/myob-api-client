require 'myob/api/result_iterator'
require 'myob/api/models/customer'
require 'myob/api/models/supplier'



module Myob
  module Api
    class Contact


      def initialize(myob)
        @myob = myob
      end

      def customers
        ResultIterator.new(@myob, Myob::Api::Models::Customer, 'Contact/Customer/')
      end

      def suppliers
        ResultIterator.new(@myob, Myob::Api::Models::Supplier, 'Contact/Supplier/')
      end
    end
  end
end
