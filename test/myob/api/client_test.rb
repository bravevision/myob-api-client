require 'test_helper'

class Myob::Api::ClientTest < Minitest::Test
  def test_that_it_has_a_version_number
    refute_nil ::Myob::Api::Client::VERSION
  end

  def test_that_can_init_and_load_items

    myob = Myob::Api::Client.new(uri: ENV['TEST_MYOB_URI'] , password: ENV['TEST_MYOB_PASSWORD'], uuid: ENV['TEST_MYOB_UUID'])

    count = 0
    myob.inventory.items.limit(10).each do |item|
      # puts item.name, item.uid
      count += 1
    end

    assert_equal(10, count)

  end

  def test_can_show_customers
    myob = Myob::Api::Client.new(uri: ENV['TEST_MYOB_URI'] , password: ENV['TEST_MYOB_PASSWORD'], uuid: ENV['TEST_MYOB_UUID'])
    count = 0
    myob.contact.customers.limit(10).each do |item|
      # puts item.uid
      count += 1
    end

    assert_equal(10, count)
  end

  def test_can_show_suppliers
    myob = Myob::Api::Client.new(uri: ENV['TEST_MYOB_URI'] , password: ENV['TEST_MYOB_PASSWORD'], uuid: ENV['TEST_MYOB_UUID'])
    count = 0
    myob.contact.suppliers.limit(10).each do |item|
      # puts item.uid
      count += 1
    end

    assert_equal(10, count)
  end

  def test_can_create_product
    # myob = Myob::Api::Client.new(uri: ENV['TEST_MYOB_URI'] , password: ENV['TEST_MYOB_PASSWORD'], uuid: ENV['TEST_MYOB_UUID'])
    # item                 = Myob::Api::Models::InventoryItem.new(myob)
    # item.name            = "test item"
    # item.number          = rand(0..9000000)
    # item.description     = "test : description"
    # item.is_active       = true
    # item.use_description = true
    # item.is_inventoried  = true
    # item.is_sold         = true
    # item.is_bought       = true
    # item.selling_details.base_selling_price = 1.0
    # item.selling_details.tax_code_uid             = '4da9e552-ba5b-4634-82d6-92144e4961a9'
    # item.buying_details.tax_code_uid             = '4da9e552-ba5b-4634-82d6-92144e4961a9'
    # item.cost_of_sales_uid        = '7201d6a1-7128-4903-851c-6e49d52b5e02'
    # item.income_account_uid       = '51533e67-b709-46bd-91bd-19497a2ed46e'
    # item.asset_account_uid        = '035a9716-bac3-4450-baff-f25f3f49ab47'
    # item.save
    # assert(item.persisted)
    # assert(!item.uid.nil?)

  end

  def test_can_create_order
    # myob = Myob::Api::Client.new(uri: ENV['TEST_MYOB_URI'] , password: ENV['TEST_MYOB_PASSWORD'], uuid: ENV['TEST_MYOB_UUID'])
    # item                 = Myob::Api::Models::SaleOrderItem.new(myob)
    # item.date            = "2016-09-21 00:00:00"
    # item.customer        = {
    #     uid: 'c5fb6c3c-b31a-4ffb-bbc6-20e441b43c0f'
    # }
    # item.is_tax_inclusive = false
    # item.customer_purchase_order_number = '45394875'
    # item.delivery_status  = 'Email'
    # item.lines  = [
    #     {
    #         Type: 'Transaction',
    #         Item: {
    #           UID: '5f365b7d-4489-4c76-a4ef-399e212746dd'
    #         },
    #         TaxCode: {
    #             UID: '4da9e552-ba5b-4634-82d6-92144e4961a9',
    #             Code: 'GST'
    #         }
    #     },
    #     {
    #         Type: 'Header',
    #         Description: 'test'
    #     }
    # ]
    # # item.is_active       = true
    # # item.base_selling_price = 1.0
    # item.save
    # assert(item.persisted)
    # assert(!item.uid.nil?)
  end

  def test_update_item
    myob = Myob::Api::Client.new(uri: ENV['TEST_MYOB_URI'] , password: ENV['TEST_MYOB_PASSWORD'], uuid: ENV['TEST_MYOB_UUID'])
    item = myob.inventory.find_item('44c73e06-6e8e-4869-9308-900196bae146')
    item.custom_field_2 ||= ''
    item.custom_field_2 << 'E'
    puts "Updating product"
    item.save
  end

  def test_change_price
    price = rand(0..100)

    myob = Myob::Api::Client.new(uri: ENV['TEST_MYOB_URI'] , password: ENV['TEST_MYOB_PASSWORD'], uuid: ENV['TEST_MYOB_UUID'])
    item = myob.inventory.find_item('44c73e06-6e8e-4869-9308-900196bae146')
    item.selling_details.base_selling_price = price
    puts "Updating product"
    item.save

    new_item = myob.inventory.find_item('44c73e06-6e8e-4869-9308-900196bae146')

    assert(new_item.selling_details.base_selling_price == price)
  end

  def test_update_item_price_matrix
    # myob = Myob::Api::Client.new(uri: ENV['TEST_MYOB_URI'] , password: ENV['TEST_MYOB_PASSWORD'], uuid: ENV['TEST_MYOB_UUID'])
    # item = myob.inventory.find_item_price_matrix('44c73e06-6e8e-4869-9308-900196bae146')
    # item.selling_prices.first[:Levels][:LevelF] = 123.45
    # item.save
  end

  def test_find_item_price_matrix
    # myob = Myob::Api::Client.new(uri: ENV['TEST_MYOB_URI'] , password: ENV['TEST_MYOB_PASSWORD'], uuid: ENV['TEST_MYOB_UUID'])
    # item = myob.inventory.find_item_price_matrix('44c73e06-6e8e-4869-9308-900196bae146')
    # item.selling_prices.first[:Levels][:LevelF] = 2.2
    # item.save
  end
  def test_can_show_order
    # myob = Myob::Api::Client.new(uri: ENV['TEST_MYOB_URI'] , password: ENV['TEST_MYOB_PASSWORD'], uuid: ENV['TEST_MYOB_UUID'])
    #
    # count = 0
    # myob.order.sale_order_item.limit(2).each do |item|
    #   puts item.customer[:name]
    #   count += 1
    # end
    #
    # assert_equal(1, count)
  end

  def test_can_show_tax_codes
    myob = Myob::Api::Client.new(uri: ENV['TEST_MYOB_URI'] , password: ENV['TEST_MYOB_PASSWORD'], uuid: ENV['TEST_MYOB_UUID'])
    count = 0
    myob.tax_codes.each do |tax_code|
      count += 1
    end
  end

  def test_can_show_restocking
    myob = Myob::Api::Client.new(uri: ENV['TEST_MYOB_URI'] , password: ENV['TEST_MYOB_PASSWORD'], uuid: ENV['TEST_MYOB_UUID'])
    item = myob.inventory.find_item('44c73e06-6e8e-4869-9308-900196bae146')
  end
end
